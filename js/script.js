const skillsButton = document.querySelector('.info__button__skills');
const portfolioItems = document.querySelector('.info__portfolio__items');
const portfolioButton = document.querySelector('.info__button__portfolio');


skillsButton.addEventListener("click", () => {
    const skillsText = document.createElement("div");
    skillsText.innerHTML =
        "My skills:<br/> HTML5<br/> CSS3<br/> Bootstrap<br/> jQuery<br/> SASS/SCSS<br/> Java Script<br/> Photoshop<br/> Figma<br/> Illustrator<br/> React<br/> Redux<br/> Git";
    portfolioItems.classList.remove("grid");
    portfolioItems.classList.add("no-grid");
    skillsText.style.display = "flex";
    skillsText.style.flexDirection = "column";
    skillsText.style.justifyContent = "center";
    skillsText.style.alignItems = "center";
    portfolioItems.innerHTML = "";
    portfolioItems.appendChild(skillsText);
});

portfolioButton.addEventListener("click", () => {
    portfolioItems.innerHTML = `<div class="info__portfolio__item"></div> 
    <div class="info__portfolio__item"></div> 
    <div class="info__portfolio__item"></div> 
    <div class="info__portfolio__item"></div> 
    <div class="info__portfolio__item"></div> 
    <div class="info__portfolio__item"></div>`;
    portfolioItems.classList.remove("no-grid");
    portfolioItems.classList.add("grid");
});

const contactButton = document.querySelector(".info__button__contact");
contactButton.addEventListener("click", () => {
    window.location.href = "mailto:valeriia.berestenko@gmail.com";
});

const downloadButton = document.querySelector('.info__button__CV');

downloadButton.addEventListener('click', () => {
    const pdfUrl = './CV_Valeriia_Berestenko.pdf';
    const fileName = 'Valeriia_Berestenko_CV.pdf';
    fetch(pdfUrl)
        .then(response => response.blob())
        .then(blob => {
            const link = document.createElement('a');
            link.href = window.URL.createObjectURL(new Blob([blob]));
            link.download = fileName;
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        });
});

///////////////////////////////////////////////////////

const dayNightImg = document.querySelector('.day_night__img');
const html = document.querySelector('html');
const socialImgs = document.querySelectorAll('.info__name__social');

dayNightImg.addEventListener('click', function () {
    if (html.classList.contains('night')) {
        html.classList.remove('night');
        socialImgs.forEach(img => {
            let src = img.src;
            img.src = src.replace('-night', '');
        });
    } else {
        html.classList.add('night');
        socialImgs.forEach(img => {
            let src = img.src;
            img.src = src.replace('.png', '-night.png');
        });
    }
});



